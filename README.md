# Introducción

Este repositorio contiene el código referente a un ejemplo de uso de la clase Crypto Class que se menciona en la entrada del blog [Cómo almacenar información confidencial en Salesforce (secretos)](http://forcegraells.com/2018/12/29/almacenar-secretos-salesforce/).

Se utilizan las funciones:

1. Crypto.encryptWithManagedIV()
1. Crypto.decryptWithManagedIV()

La generación del Vector de Inicialización y de la clave AES-128 se delega en Salesforce. Es importante **destacar** que no debe almacenarse nunca la clave en el código, sinó que debe ser externalizada a un *Protected Custom Setting* ó preferentemente a un *Protected Custom Metadata Type*.

