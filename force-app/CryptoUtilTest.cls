@isTest
public with sharing class CryptoUtilTest {
    
    public CryptoUtilTest() {
        test1();
    }

    @isTest static void test1(){

        final String secreto = 'Los reyes son los padres';

        CryptoUtil crypto = new CryptoUtil();
        
        Blob secretoEncriptado = crypto.encriptar(secreto);
        String secretoDesencriptado = crypto.desencriptar(secretoEncriptado);

        System.assert(secreto.equals(secretoDesencriptado));

    }
}

