public with sharing class CryptoUtil {
    
    private final String secreto = 'Los reyes son los padres'; 
	private final Blob secretoBl = Blob.valueOf(secreto); 
	
	//Salesforce genera una clave de encriptación
	private Blob clave = Crypto.generateAesKey(128); 
	
	public Blob encriptar(String secreto) {

		System.debug('-ege- Secreto a encriptar: ' + secretoBl.toString());
		
		//Encriptación del secreto, indicando a Salesforce que proporcione un Inizialization Vector interno
		Blob secretoBl =  Crypto.encryptWithManagedIV('AES128', clave, Blob.valueOf(secreto)); 
		
		System.debug('-ege- Secreto encriptado: ' + EncodingUtil.base64Encode(secretoBl));
		
		return secretoBl;
	}
	
	public String desencriptar(Blob secretoEncriptado) {
		
		//Desencriptar un secreto con la clave utilizada anteriormente, y el Vector que haya proporcionado Salesforce
		Blob secretoDesencriptado = Crypto.decryptWithManagedIV('AES128', clave, secretoEncriptado); 

		System.debug('-ege- Secreto Desencriptado: ' + secretoDesencriptado.toString());
		
		return secretoDesencriptado.toString(); 
	}

}

